<?php

/**
* DESARROLLADO POR (SISTEDS.COM)
*/

defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'libraries/REST_Controller.php');
use Restserver\libraries\REST_Controller;

class Usuario extends REST_Controller{

  public function __construct(){
    header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS");
    header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");
    header("Access-Control-Allow-Origin: *");
    parent::__construct();
    //Codeigniter : Write Less Do More
  }
 
  /**FUNCION PRINCIPAL*/
  public function index_get(){
    $this->response(array('standard'=>1));
  }

  /**FUNCION RETORNA EL TOTAL DE USUARIOS*/
  public function getCountUser_get(){
    $query = "select count(usuario) total from _usuario";
    $lista = $this->GlobalMod->query($query);
    $this->response($lista);
  }

    /**FUNCION RETORNA EL TOTAL DE PAISES REGISTRADOS*/
  public function getCountCountry_get(){
   $query = "select count(country)total,country  from _usuario group by country order by 1 desc";
    $lista = $this->GlobalMod->query($query);
    $this->response($lista);
  }


  /**FUNCION PERMITE MODIFICAR LOS DATOS DEL USUARIO*/
  public function update_user_post(){
    $data = $this->post();
    $where = array('id_usuario' =>$data['iduserkey']);
    $set = array('email'=>$data['email'],
      'idioma'=>$data['idioma'],
      'perfilimg'=>$data['perfilimg'],
      'urlfb'=>$data['urlfb'],
      'urltw'=>$data['urltw'],
      'urllk'=>$data['urllk'],
      'typeconect'=>$data['typeconect'],
      'sitiodecontacto'=>$data['sitiodecontacto'],
      'celular'=>$data['celular']);
    $this->GlobalMod->proc_update($set,'_usuario',$where);
  }

  /**FUNCION DEVUELVE LOS VALORES DEL USUARIO*/
  public function get_perfile_post(){
    $data = $this->post();
    $where = array('id_usuario' =>$data['iduserkey']);
    $list = $this->GlobalMod->get_list_Where('*','_usuario',$where);
    $this->response($list);
  }

  /**FUNCION CONTROL DE ACCESO AL SISTEMA*/
  public function access_user_post(){
    $data = $this->post();
    $info['user'] = $data['user'];
    $info['password'] = $data['password'];
    $where = array('usuario' =>$data['user'] ,'clave'=>sha1(md5(sha1($data['password']))));
    $list = $this->GlobalMod->get_list_Where('*','_usuario',$where);
    if(count($list)>0){$this->response($list);}
    else{
      $this->response(array('status'=>404));
    }
  }

  /**FUNCION REGISTRO DE NUEVO USUARIO*/
  public function save_registr_post(){
    $data = $this->post();
    $info['usuario'] = $data['usuario'];
    $info['clave'] = $data['clave'];
    $info['tokens'] = sha1(time());
    $info['email'] = $data['email'];
    $info['idioma'] = $data['idioma'];
    $info['estadoonline'] = "00";
    $info['perfilimg'] = null;
    $info['urlfb'] = null;
    $info['urltw'] = null;
    $info['urllk'] = null;
    $info['typeconect'] = null;
    $info['sitiodecontacto'] = null;
    $info['celular'] = null;
    $info['country'] = $data['country'];
    //$this->message_client($info['email']);
    $datos_insert = array('id_usuario' =>null ,
    'usuario'=>$info['usuario'],
    'clave'=>sha1(md5(sha1($info['clave']))),
    'tokens'=>$info['tokens'],
    'email'=>$info['email'],
    'idioma'=>$info['idioma'],
    'estadoonline'=>$info['estadoonline'],
    'perfilimg'=>$info['perfilimg'],
    'urlfb'=>$info['urlfb'],
    'urltw'=>$info['urltw'],
    'urllk'=>$info['urllk'],
    'typeconect'=>$info['typeconect'],
    'sitiodecontacto'=>$info['sitiodecontacto'],
    'celular'=>$info['celular'],
    'country'=>$info['country']
  );
    $idinsert = $this->GlobalMod->proc_insert_id($datos_insert,'_usuario');
    //$this->response($info);
    $strHtml = '<div style="height: 60px;text-align:center;background-color: #00bf9a;width: 100%;color:#FFF">
<br>
  CONFIRMACION DE REGISTRO <strong>AISSACK</strong>
</div>
<div style="width: 100%">
  <p style="font-family: Calibri;font-size:17px;color:#000">Gracias por registrarse en nuestra plataforma de publicaciones referente a la informatica.
  <b><a href="">Aissack</a></b> desarrollado por <b><a href="http://sisteds.com">Sisteds</a></b> confirma el correo electronico 
   para su activacion y la restauracion de su contraseña si logra olvidarla.
  </p>
    <br>
  <a href="'.base_url().'Email/Confirm/'.$info['tokens'].'/'.$idinsert.'" style="text-decoration:none;background-color: #00bf9a;color:#FFF;padding: 6px;border-radius: 3px 3px 3px 3px">C O N F I R M A R</a>
</div>';
$title    = 'Aissack.com Confirmacion de registro'.date('Y-m-d');
$heades = 'MIME-Version: 1.0'. "\r\n" .
        'Content-Type: text/html; charset=ISO-8859-1'. "\r\n";
          $strHtml = wordwrap($strHtml, 70, "\r\n");
          mail($info['email'], $title, $strHtml, $heades);
     $this->response(array('message'=>$info['email']));
  }




  /**FUNCION MODIFICA EL PERFIL  DEL NUEVO USUARIO*/
  public function save_update_post(){
    $data = $this->post();
    $info['email'] = $data['email'];
    $info['idioma'] = $data['idioma'];
    $info['estadoonline'] = $data['estadoonline'];
    $info['perfilimg'] = null;
    $info['urlfb'] = null;
    $info['urltw'] = null;
    $info['urllk'] = null;
    $info['typeconect'] = null;
    $info['sitiodecontacto'] = null;
    $info['celular'] = null;
    //$this->message_client($info['email']);
    $datos_update = array(
    'email'=>$data['email'],
    'idioma'=>$data['idioma'],
    'estadoonline'=>$data['estadoonline'],
    'perfilimg'=>$data['perfilimg'],
    'urlfb'=>$data['urlfb'],
    'urltw'=>$data['urltw'],
    'urllk'=>$data['urllk'],
    'typeconect'=>$data['typeconect'],
    'sitiodecontacto'=>$data['sitiodecontacto'],
    'celular'=>$data['celular']
  );

    $where = array('id_usuario' =>$data['id_usuario'] );
    $this->GlobalMod->proc_update($datos_update,'_usuario',$where);
    $this->response($info);
  }


  /**FUNCION ENVIA CORREO ELECTRONICO AL USUARIO REGISTRADO*/
  public function message_client($email=''){
    $titulo    = ''.date('Y-m-d');
    $mensaje   = '<html></html>';
    $cabeceras = 'MIME-Version: 1.0'. "\r\n" .
        'Content-Type: text/html; charset=ISO-8859-1'. "\r\n";
          $mensaje = wordwrap($mensaje, 70, "\r\n");
          mail($email, $titulo, $mensaje, $cabeceras);
     $this->response(array('estado'=>$email));

  }


}
