<?php 
/**
* DESARROLLADO POR (SISTEDS.COM)
*/
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'libraries/REST_Controller.php');
use Restserver\libraries\REST_Controller;

class Favorite extends REST_Controller{
	
public function __construct(){
    header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS");
    header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");
    header("Access-Control-Allow-Origin: *");
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

/**FUNCION PRINCIPAL*/
public function index_get(){
    $this->response(array('standard'=>1));
  }

/**FUNCION AGREGA A LA LISTA DE FAVORITOS*/
public function add_favorite_post(){
	$data = $this->post();
	$data_to_insert = array('id_favorito' =>null ,'id_usuario'=>$data['idusuario'],'id_proyecto'=>$data['idproyecto']);
	$bo = false;
	$show['id_insert'] = $this->GlobalMod->proc_insert_id($data_to_insert,'_favorito');
	$show['key_tokens'] = md5(date('Y-m-d h:m:s'));
	$bo = true;
	$show['status'] = $bo;
	$this->response($show);
}


/**FUNCION REMOVER FAVORITOS DE LA LISTA DE USUARIOS*/
public function remover_favorite_post(){
	$data = $this->post();
	$query = "delete from _favorito where id_favorito=".$data['idfavorite'];
	$this->GlobalMod->queryInsert($query);
	$this->response(array('status'=>202));
}


/**FUNCION QUE PERMITE LISTAR LOS FAVORITOS MARCADOS POR EL USUARIO*/
public function list_favorite_post(){
	$data = $this->post();
	$query = "select * from _proyecto p inner join _favorito f on p.id_proyecto = f.id_proyecto where id_usuario=".$data['iduser'];
	$list = $this->GlobalMod->query($query);
	$this->response($list); 
}

/**FUNCION QUE VERIFICA EL PRODUCTO ESTE EN FAVORITO*/
public function verify_favorite_post(){
	$data = $this->post();
	$idproyect=$data['idproyect'];
	$iduser=$data['id_usuario'];
	$where =  array('id_usuario' => $iduser,'id_proyecto'=>$idproyect );
	$list = $this->GlobalMod->get_list_where('*','_favorito',$where);
	$dataresponse['status']=404;
	$dataresponse['idfavorite']=-1;
	if(count($list)>0){
		$dataresponse['status']=202;
		$dataresponse['idfavorite']=$list[0]->id_favorito;
	}
	$this->response($dataresponse);
}



}