<?php 
/**
* DESARROLLADO POR (SISTEDS.COM)
*/

defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'libraries/REST_Controller.php');
use Restserver\libraries\REST_Controller;

class Employe extends REST_Controller {
	
public function __construct(){
    header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS");
    header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");
    header("Access-Control-Allow-Origin: *");
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

/**FUNCION PRINCIPAL*/
public function index_get(){
    $this->response(array('standard'=>1));
  }


public function list_index_post(){
	$data = $this->post();
	$page = $data['numberpage']*10;
	$query = "select e.*,u.usuario,u.perfilimg from _empleo e  inner join _usuario u on e._usuario_id_usuario = u.id_usuario order by 1 desc limit ".$page.",10 ";
	$list = $this->GlobalMod->query($query);
	$this->response($list);
}



}