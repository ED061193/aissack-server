<?php 
/**
* DESARROLLADO POR (SISTEDS.COM)
*/
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'libraries/REST_Controller.php');
use Restserver\libraries\REST_Controller;


class Payment extends REST_Controller{
	
public function __construct(){
    header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS");
    header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");
    header("Access-Control-Allow-Origin: *");
    parent::__construct();
    //Codeigniter : Write Less Do More
  }
/**
	Stripe, Paypal, paysafe, openpay, epayco, payulatam, payvalida, Mercadopago
*/
/**FUNCION PRINCIPAL*/
public function index_get(){
    $this->response(array('standard'=>1));
  }


/**FUNCION QUE PERMITE REGISTRAR NUEVO REGISTRO EN PLAN DE PAGO*/
public function insert_newplan_post(){
	$data = $this->post();
	$data_to_insert = array('idplan_pago' =>null
		'description'=>$data['description'],
		'n_days'=>$data['n_days'],
		'statuts_view'=>1,
		'price'=>$data['price'],
		'date_insert'=>date('Y-m-d'));
	$bo = false;
	$show['id_insert'] = $this->GlobalModule->proc_insert_id($data_to_insert,'plan_pago');
	$show['key_tokens'] = md5(date('Y-m-d h:m:s'));
	$bo = true;
	$show['status'] = $bo;
	$this->response($show);
 }



/**FUNCION QUE PERMITE MODIFICAR EL REGISTRO EN PLAN DE PAGO*/
public function update_plan_post(){
	$data = $this->post();
	$data_to_update = array(
		'description'=>$data['description'],
		'n_days'=>$data['n_days'],
		'statuts_view'=>1,
		'price'=>$data['price'],
		'date_insert'=>date('Y-m-d'));
	$where = array('idplan_pago' =>$data['idplan_payment']);
	$bo = false;
	$show['id_insert'] = $this->GlobalModule->proc_update($data_to_update,'plan_pago',$where);
	$show['key_tokens'] = md5(date('Y-m-d h:m:s'));
	$bo = true;
	$show['status'] = $bo;
	$this->response($show);
 }

/**FUNCION QUE PERMITE (ELIMINAR)MODIFICAR EL REGISTRO EN PLAN DE PAGO*/
public function delete_plan_post(){
	$data = $this->post();
	$data_to_update = array(
		'statuts_view'=>0,
		'date_insert'=>date('Y-m-d'));
	$where = array('idplan_pago' =>$data['idplan_payment']);
	$bo = false;
	$show['id_insert'] = $this->GlobalModule->proc_update($data_to_update,'plan_pago',$where);
	$show['key_tokens'] = md5(date('Y-m-d h:m:s'));
	$bo = true;
	$show['status'] = $bo;
	$this->response($show);
 }

/**FUNCION DE LISTA TODOS LOS PLANES DE PAGOS DISPONIBLES*/
public function list_plan_get(){
	$where =  array('statuts_view' => 1 );
	$list = $this->GlobalMod->get_list_where('*','plan_pago',$where);
	$this->response($list);
}

}