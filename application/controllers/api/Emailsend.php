<?php 

/**
* DESARROLLADO POR (SISTEDS.COM)
*/

defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'libraries/REST_Controller.php');
use Restserver\libraries\REST_Controller;

class Emailsend extends REST_Controller {
	
public function __construct(){
    header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS");
    header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");
    header("Access-Control-Allow-Origin: *");
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

/**FUNCION PRINCIPAL*/
public function index_get(){
    $this->response(array('standard'=>1));
  }

public function contact_post(){
	$data = $this->post();
	$email_details = $data['email_details'];
	$email_details_hidden = $data['email_details_hidden'];
	$telephonodetails = $data['telephonodetails'];
	$messagedetails = $data['messagedetails'];
	$idproduct = $data['idproduct'];
	$routeimage = $data['routeimage'];
	$nameproyect = $data['nameproyect'];
	$strHtml = '<h4 style="font-family:Calibri;font-size: 20px">'.$nameproyect.'</h4>
	<div style="width: 100%;display: inline-block;">
	<div style="vertical-align: top">
	    <a href="http://aissack.com/dsoftware/'.$idproduct.'">
		<img src="http://aissack.com/'.$routeimage.'" style="width:150px"></a>
	</div>
	<div style="vertical-align: top;font-family: Calibri">
		<h3>Solicitud de informacion</h3><hr>
		<b>De:</b> '.$email_details.'<br>
		<b>Telefono:</b> '.$telephonodetails.'  <br>
		<b>Mensaje:</b> '.$messagedetails.'
	</div>
</div>';
$title    = 'Aissack.com ('.$nameproyect.')'.date('Y-m-d');
$heades = 'MIME-Version: 1.0'. "\r\n" .
        'Content-Type: text/html; charset=ISO-8859-1'. "\r\n";
          $strHtml = wordwrap($strHtml, 70, "\r\n");
          mail($email_details, $title, $strHtml, $heades);
     $this->response(array('message send to'=>$email_details));
}



}