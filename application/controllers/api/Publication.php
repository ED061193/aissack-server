<?php 
/**
* DESARROLLADO POR (SISTEDS.COM)
*/

defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'libraries/REST_Controller.php');
use Restserver\libraries\REST_Controller;

class Publication extends REST_Controller {

public function __construct(){
    header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS");
    header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");
    header("Access-Control-Allow-Origin: *");
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

/**FUNCION PRINCIPAL*/
public function index_get(){
    $this->response(array('standard'=>1));
  }





/**FUNCION REGISTRA NUEVA PUBLICACION REGISTRO DE PROYECTO*/
public function new_publication_proyect_post(){
	$data = $this->post();
	$data_to_insert = array('id_proyecto' => null,
	 'nombre'=>$data['name'],
	 'descripcion'=>$data['description'],
	 'precio'=>$data['price'],
	 'etiquetas'=>$data['tags'],
	 'fechapub'=>date('Y-m-d'),
	 'urldemo'=>$data['url_demo'],
	 'upload'=>$data['download'],
	 'tipoproyecto'=>null,
	 'soporte'=>null,
	 'imgmaster'=>null,
	 'img1'=>null,
	 'img2'=>null,
	 'img3'=>null,
	 'terminos'=>true,
	 'usuariofk'=>$data['key_user'],
	 'oferta'=>null,
	 'modulo'=>null,
	 'nivel'=>null,
	 'estadoborrador'=>0,
	 'estadoview'=>1
	 );
	$bo = false;
	$show['id_insert'] = $this->GlobalMod->proc_insert_id($data_to_insert,'_proyecto');
	$show['key_tokens'] = md5(date('Y-m-d h:m:s'));
	$bo = true;
	$show['status'] = $bo;
	$this->response($show);

}

/**FUNCION MODIFICA EL REGISTRO DEL PROYECTO*/
public function update_publication_proyect_post(){
	$data = $this->post();
	$data_to_update = array(
	 'nombre'=>$data['name'],
	 'descripcion'=>$data['description'],
	 'precio'=>$data['price'],
	 'etiquetas'=>$data['tags'],
	 'fechapub'=>date('Y-m-d'),
	 'urldemo'=>$data['demo'],
	 'upload'=>$data['download'],
	 /*'tipoproyecto'=>$data['type_proyecto'],
	 'soporte'=>$data['sopport'],
	 'imgmaster'=>$data['img_main'],
	 'img1'=>$data['img1'],
	 'img2'=>$data['img2'],
	 'img3'=>$data['img3'],
	 'terminos'=>$data['termino'],
	 'usuariofk'=>$data['key_user'],
	 'oferta'=>$data['oferta'],
	 'modulo'=>$data['module'],
	 'nivel'=>$data['level'],
	 'estadoborrador'=>$data['status']*/
	 );
	$where = array('id_proyecto' => $data['id_proyecto']);
	$bo = false;
	$show['id_insert'] = $this->GlobalMod->proc_update($data_to_update,'_proyecto',$where);
	$show['key_tokens'] = md5(date('Y-m-d h:m:s'));
	$bo = true;
	$show['status'] = $bo;
	$this->response($show);

}


/**FUNCION ELIMINAR(CAMBIO DE ESTADO DESDE EL CLIENTE) EL REGISTRO DEL PROYECTO*/
public function delete_publication_proyect_post(){
	$data = $this->post();
	$data_to_update = array(
	 'estadoborrador'=>'delete'
	 );
	$where = array('id_proyecto' => $data['proyect_id']);
	$bo = false;
	$show['id_insert'] = $this->GlobalMod->proc_update($data_to_update,'_proyecto',$where);
	$show['key_tokens'] = md5(date('Y-m-d h:m:s'));
	$bo = true;
	$show['status'] = $bo;
	$this->response($show);

}


/**FUNCION REGISTRA NUEVA PUBLICACION REGISTRO DE EMPLEO*/
public function new_publication_employe_post(){
	$data = $this->post();
	$data_to_insert = array('id_empleo' => null,
		'titulo'=>$data['title'],
		'descripcionresumen'=>$data['details'],
		'descripciondetalle'=>$data['description_details'],
		'estadoproceso'=>$data['status_progress'],
		'estadoview'=>$data['status_view'],
		'fecharegistro'=>$data['date_registre'],
		'salario'=>$data['salario'],
		'_usuario_id_usuario'=>$data['key_user']
		);
	$bo = false;
	$show['id_insert'] = $this->GlobalMod->proc_insert_id($data_to_insert,'_empleo');
	$show['key_tokens'] = md5(date('Y-m-d h:m:s'));
	$bo = true;
	$show['status'] = $bo;
	$this->response($show);
}



/**FUNCION MODIFICA LA PUBLICACION DEL REGISTRO DE EMPLEO*/
public function update_publication_employe_post(){
	$data = $this->post();
	$data_to_update = array(
		'titulo'=>$data['title'],
		'descripcionresumen'=>$data['details'],
		'descripciondetalle'=>$data['description_details'],
		'estadoproceso'=>$data['status_progress'],
		'estadoview'=>$data['status_view'],
		'fecharegistro'=>$data['date_registre'],
		'salario'=>$data['salario'],
		'_usuario_id_usuario'=>$data['key_user']
		);
	$where = array('id_empleo' => $data['key_employe']);
	$bo = false;
	$show['id_insert'] = $this->GlobalMod->proc_update($data_to_update,'_empleo',$where);
	$show['key_tokens'] = md5(date('Y-m-d h:m:s'));
	$bo = true;
	$show['status'] = $bo;
	$this->response($show);
}

/**FUNCION ELIMINAR(UPDATE ESTADO DESDE EL CLIENTE) LA PUBLICACION DEL REGISTRO DE EMPLEO*/
public function delete_publication_employe_post(){
	$data = $this->post();
	$data_to_update = array(
		'estadoview'=>'delete'
		);
	$where = array('id_empleo' => $data['key_employe']);
	$bo = false;
	$show['id_insert'] = $this->GlobalMod->proc_update($data_to_update,'_empleo',$where);
	$show['key_tokens'] = md5(date('Y-m-d h:m:s'));
	$bo = true;
	$show['status'] = $bo;
	$this->response($show);
}


}