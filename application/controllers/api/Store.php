<?php 

/**
* DESARROLLADO POR (SISTEDS.COM)
*/

defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'libraries/REST_Controller.php');
use Restserver\libraries\REST_Controller;

class Store extends REST_Controller {
	
public function __construct(){
    header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS");
    header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");
    header("Access-Control-Allow-Origin: *");
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

/**FUNCION PRINCIPAL*/
public function index_get(){
    $this->response(array('standard'=>1));
  }

public function list_my_software_post(){
	$data = $this->post();
	$where = array('usuariofk'=>$data['idusuario'],'estadoview'=>1);
	$list = $this->GlobalMod->get_list_where('*','_proyecto',$where);
	$this->response($list);
}


/**FUNCION QUE LISTA TODOS LOS SOFTWARE PUBLICADOS*/
public function list_storage_post(){
	$data = $this->post();
	$numberpage = $data['numberpage'];
	$numberpage = $numberpage*8;
	$query = "select p.*,u.usuario FROM _proyecto p inner join _usuario u on u.id_usuario = p.usuariofk  where estadoview=1 order by 1 desc limit ".$numberpage.",8 ";//20
	$list = $this->GlobalMod->query($query);
	$this->response($list);
	
}

public function search_like_post(){
	$data = $this->post();
	$text = $data['text'];
	if($text==""){
		$queryOpc2 = "select nombre as title , id_proyecto as id , etiquetas as label , usuariofk as fkusers
from _proyecto where estadoview=1 order by 1 desc limit 100";
	}else{
		$queryOpc2 = "select nombre as title , id_proyecto as id from _proyecto where estadoview=1 and  descripcion like '%".$text."%'";
	}
	
	$list = $this->GlobalMod->query($queryOpc2);
	if(count($list)==0){
		$queryOpc2 = "select nombre as title , id_proyecto as id , etiquetas as label , usuariofk as fkusers
from _proyecto where estadoview=1 order by 1 desc limit 100";
		$list = $this->GlobalMod->query($queryOpc2);
	}
	$this->response($list);
}

public function list_storage_top_post(){
	$data = $this->post();
	$query = "select p.*,u.usuario FROM _proyecto p inner join _usuario u on u.id_usuario = p.usuariofk  where estadoview=1 order by 1 desc limit 10 ";//20
	$list = $this->GlobalMod->query($query);
	$this->response($list);
	
}

public function list_details_software_post(){
	$data = $this->post();
	$idsoftware = $data['idsoftware'];
	$query = "select * from _proyecto p inner join _usuario u
 on u.id_usuario=p.usuariofk where estadoview=1 and id_proyecto=".$idsoftware." order by 1 desc";
 	$list = $this->GlobalMod->query($query);
 	$dataresponse['status']=404;
 	if(count($list)>0){
 		$dataresponse['status']=202;
 		$dataresponse['list']=$list;
 	}
 	$this->response($dataresponse);

}


}




  